<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200924043630 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE schedule (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, company_code VARCHAR(255) NOT NULL, schedule_code VARCHAR(255) NOT NULL, schedule_description LONGTEXT DEFAULT NULL, next_runtime DATETIME NOT NULL, last_runtime DATETIME DEFAULT NULL, frequency VARCHAR(255) NOT NULL, action VARCHAR(255) NOT NULL, action_url VARCHAR(255) NOT NULL, action_params LONGTEXT NOT NULL, action_http_method VARCHAR(50) NOT NULL, created_at DATETIME NOT NULL, last_updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE schedule_history (id INT AUTO_INCREMENT NOT NULL, schedule_id INT NOT NULL, created_at DATETIME NOT NULL, result_status VARCHAR(255) NOT NULL, response_code VARCHAR(255) NOT NULL, response_message LONGTEXT NOT NULL, INDEX IDX_AAD05CFAA40BC2D5 (schedule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE schedule_history ADD CONSTRAINT FK_AAD05CFAA40BC2D5 FOREIGN KEY (schedule_id) REFERENCES schedule (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE schedule_history DROP FOREIGN KEY FK_AAD05CFAA40BC2D5');
        $this->addSql('DROP TABLE schedule');
        $this->addSql('DROP TABLE schedule_history');
    }
}

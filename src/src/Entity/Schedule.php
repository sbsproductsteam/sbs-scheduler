<?php

namespace App\Entity;

use App\Repository\ScheduleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ScheduleRepository::class)
 */
class Schedule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $company_code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $schedule_code;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $schedule_description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $next_runtime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $last_runtime;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $frequency;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $action;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $action_url;

    /**
     * @ORM\Column(type="text")
     */
    private $action_params;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $action_HTTP_Method;

    /**
     * @ORM\OneToMany(targetEntity=ScheduleHistory::class, mappedBy="schedule", orphanRemoval=true)
     */
    private $schedule_histories;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $last_updated_at;


    public function __construct()
    {
        $this->schedule_histories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyCode()
    {
        return $this->company_code;
    }

    /**
     * @param mixed $company_code
     * @return Schedule
     */
    public function setCompanyCode($company_code)
    {
        $this->company_code = $company_code;

        return $this;
    }



    public function getScheduleDescription(): ?string
    {
        return $this->schedule_description;
    }

    public function setScheduleDescription(?string $schedule_description): self
    {
        $this->schedule_description = $schedule_description;

        return $this;
    }

    public function getNextRuntime(): ?\DateTimeInterface
    {
        return $this->next_runtime;
    }

    public function setNextRuntime(\DateTimeInterface $next_runtime): self
    {
        $this->next_runtime = $next_runtime;

        return $this;
    }

    public function getLastRuntime(): ?\DateTimeInterface
    {
        return $this->last_runtime;
    }

    public function setLastRuntime(?\DateTimeInterface $last_runtime): self
    {
        $this->last_runtime = $last_runtime;

        return $this;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFrequency(): ?string
    {
        return $this->frequency;
    }

    /**
     * @param mixed $frequency
     */
    public function setFrequency($frequency): self
    {
        $this->frequency = $frequency;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getScheduleCode()
    {
        return $this->schedule_code;
    }

    /**
     * @param mixed $schedule_code
     * @return Schedule
     */
    public function setScheduleCode($schedule_code)
    {
        $this->schedule_code = $schedule_code;

        return $this;
    }



    /**
     * @return Collection|ScheduleHistory[]
     */
    public function getScheduleHistories(): Collection
    {
        return $this->schedule_histories;
    }

    public function addScheduleHistory(ScheduleHistory $scheduleHistory): self
    {
        if (!$this->schedule_histories->contains($scheduleHistory)) {
            $this->schedule_histories[] = $scheduleHistory;
            $scheduleHistory->setSchedule($this);
        }

        return $this;
    }

    public function removeScheduleHistory(ScheduleHistory $scheduleHistory): self
    {
        if ($this->schedule_histories->contains($scheduleHistory)) {
            $this->schedule_histories->removeElement($scheduleHistory);
            // set the owning side to null (unless already changed)
            if ($scheduleHistory->getSchedule() === $this) {
                $scheduleHistory->setSchedule(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActionUrl()
    {
        return $this->action_url;
    }

    /**
     * @param mixed $action_url
     * @return Schedule
     */
    public function setActionUrl($action_url)
    {
        $this->action_url = $action_url;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActionParams()
    {
        return $this->action_params;
    }

    /**
     * @param mixed $action_params
     * @return Schedule
     */
    public function setActionParams($action_params)
    {
        $this->action_params = $action_params;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getActionHTTPMethod()
    {
        return $this->action_HTTP_Method;
    }

    /**
     * @param mixed $action_HTTP_Method
     * @return Schedule
     */
    public function setActionHTTPMethod($action_HTTP_Method)
    {
        $this->action_HTTP_Method = $action_HTTP_Method;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getLastUpdatedAt(): ?\DateTimeInterface
    {
        return $this->last_updated_at;
    }

    public function setLastUpdatedAt(?\DateTimeInterface $last_updated_at): self
    {
        $this->last_updated_at = $last_updated_at;

        return $this;
    }



}

<?php

namespace App\Entity;

use App\Repository\ScheduleHistoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ScheduleHistoryRepository::class)
 */
class ScheduleHistory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $result_status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $response_code;

    /**
     * @ORM\Column(type="text")
     */
    private $response_message;

    /**
     * @ORM\ManyToOne(targetEntity=Schedule::class, inversedBy="scheduleHistories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $schedule;

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getResultStatus(): ?string
    {
        return $this->result_status;
    }

    public function setResultStatus(string $result_status): self
    {
        $this->result_status = $result_status;

        return $this;
    }

    public function getSchedule(): ?Schedule
    {
        return $this->schedule;
    }

    public function setSchedule(?Schedule $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponseCode()
    {
        return $this->response_code;
    }

    /**
     * @param mixed $response_code
     * @return ScheduleHistory
     */
    public function setResponseCode($response_code)
    {
        $this->response_code = $response_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponseMessage()
    {
        return $this->response_message;
    }

    /**
     * @param mixed $response_message
     * @return ScheduleHistory
     */
    public function setResponseMessage($response_message)
    {
        $this->response_message = $response_message;

        return $this;
    }


}

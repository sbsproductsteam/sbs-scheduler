<?php


namespace App\Command;


use App\Service\ScheduleService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateSchedulesCommand extends Command
{
    protected static $defaultName = 'app:create-schedule';

    private $scheduleService;

    public function __construct(ScheduleService $scheduleService)
    {
        $this->scheduleService = $scheduleService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Create schedule jobs.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to create scheduled jobs...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln("...starting to create jobs");
        $this->scheduleService->createSchedule();
        $output->writeln("...done creating jobs");

        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;
    }
}
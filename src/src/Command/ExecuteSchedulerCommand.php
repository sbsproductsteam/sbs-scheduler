<?php
namespace App\Command;

use App\Service\EmailService;
use App\Service\ScheduleService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\WorkerService;

class ExecuteSchedulerCommand extends Command{
   // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:execute-schedule';

    private $workerService;
    private $emailService;
    private $scheduleService;

    public function __construct(WorkerService $workerService, ScheduleService $scheduleService,EmailService $emailService)
    {
        $this->workerService = $workerService;
        $this->emailService = $emailService;
        $this->scheduleService = $scheduleService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('Execute scheduled jobs.')

        // the full command description shown when running the command with
        // the "--help" option
        ->setHelp('This command allows you to execute scheduled jobs...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        $output->writeln("...starting to run jobs");
//        $this->workerService->initWorkers();
//        $this->emailService->sendTestEmail();
//        $currentDateTime = new \DateTime('now');
        $currentDateTime = new \DateTime('now');
//            $output->writeln("Current HOUR: ". $currentDateTime->format('Y-m-d H:00'));
//        $this->scheduleService->processScheduledJobs($currentDateTime->format('Y-m-d H:00'));
        $this->scheduleService->processScheduledJobs('2020-09-28 19:00');
        $output->writeln("...done running jobs");

        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        return Command::SUCCESS;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;
    } 
}
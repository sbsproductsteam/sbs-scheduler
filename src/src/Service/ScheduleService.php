<?php


namespace App\Service;


use App\Entity\Schedule;
use App\Entity\ScheduleHistory;
use Cassandra\Date;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ScheduleService
{
    private $logger;
    private $entityManager;
    private $emailService;
    private $client;


    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger, EmailService $emailService, HttpClientInterface $client)
    {

        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->emailService = $emailService;
        $this->client = $client;

    }

    /**
     * @param $data
     * @return bool
     */
    public function createScheduleFromAPI($data){
        if(!is_object($schedule = $this->getScheduleByCompanyAndScheduleCode($data['companyCode'], $data['scheduleCode']))){
            $schedule = new Schedule();
            $schedule->setCreatedAt(new \DateTime('now'));
        }else{
            $schedule->setLastUpdatedAt(new \DateTime('now'));
        }

        $schedule->setName('loan-schedule')
            ->setScheduleDescription('Default description')
            ->setCompanyCode($data['companyCode'])
            ->setScheduleCode($data['scheduleCode'])
            ->setNextRuntime(new \DateTime($data['startTime']))
            ->setFrequency($data['frequency'])
            ->setAction('call-url')
            ->setActionURL($data['actionURL'])
            ->setActionParams(json_encode($data['actionParams']))
            ->setActionHTTPMethod($data['actionHTTPMethod']);
        $this->entityManager->persist($schedule);
        $this->entityManager->flush();
        return true;
    }



    public function createSchedule(){

        $schedule = new Schedule();
        $schedule->setName('Daily schedule');
        $schedule->setScheduleDescription('Testing daily schedule');
        $schedule->setNextRuntime(new \DateTime('2020-09-16 14:00'));
        $schedule->setAction('SEND.EMAIL.GREETING');
        $schedule->setFrequency('DAILY');
        $this->entityManager->persist($schedule);
        $this->entityManager->flush();

        $schedule = new Schedule();
        $schedule->setName('Hour schedule');
        $schedule->setScheduleDescription('Testing schedule');
        $schedule->setNextRuntime(new \DateTime('2020-09-16 14:00'));
        $schedule->setAction('SEND.EMAIL.GOODBYE');
        $schedule->setFrequency('HOURLY');
        $this->entityManager->persist($schedule);
        $this->entityManager->flush();
    }

    public function processScheduledJobs($actionTime){
        $this->logger->info(("Starting processScheduledJobs() for DateTime: ".$actionTime));
        $schedules = $this->getActions($actionTime);
        foreach($schedules as $schedule){
            $this->logger->info('Name: '.$schedule->getName());
            $this->performAction($schedule);
            $this->reschedule($schedule);
        }
        $this->entityManager->flush();
    }

    private function performAction(Schedule $schedule){
        switch ($schedule->getAction()){
            case 'call-url':
                $this->executeURLCall($schedule);
                break;
            default:
                $email = $this->createEmail($schedule->getAction());
                $this->emailService->send($email);
                break;
        }

    }

    /**
     * @param Schedule $schedule
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function executeURLCall(Schedule $schedule){
        $this->logger->info(("executeURLCall()"));
        $statusCode = '';
        try{
            $response = $this->client->request(
                strtoupper($schedule->getActionHTTPMethod()),
                $schedule->getActionUrl(),
                ['json' => json_decode($schedule->getActionParams())]
            );

            $statusCode = $response->getStatusCode();
            // $statusCode = 200
            $contentType = $response->getHeaders()['content-type'][0];
            // $contentType = 'application/json'
            $content = $response->getContent();
            $this->createHistory($schedule, 'success', $statusCode, $content);
        }catch (\Exception $e){
            $this->createHistory($schedule, 'Error occurred', $statusCode, $e->getTraceAsString() );
        }

    }

    /**
     * @param Schedule $schedule
     * @param string $resultStatus
     * @param $responseCode
     * @param $responseMessage
     * @return bool
     */
    private function createHistory(Schedule $schedule, $resultStatus = 'success', $responseCode, $responseMessage){
        $scheduleHistory = new ScheduleHistory();
        $scheduleHistory->setSchedule($schedule);
        $scheduleHistory->setCreatedAt(new \DateTime('now'));
        $scheduleHistory->setResultStatus($resultStatus);
        $scheduleHistory->setResponseCode($responseCode);
        $scheduleHistory->setResponseMessage($responseMessage);
        $this->entityManager->persist($scheduleHistory);
        return true;

    }

    private function reschedule(Schedule $schedule){
        $lastRunTime = $schedule->getNextRuntime()->format('Y-m-d H:i');
        $this->logger->info('Current time: '.$lastRunTime);

        $nextRunTime = $this->generateNextRunTime($lastRunTime, $schedule->getFrequency());
        $this->logger->info('Next runtime generated '. $nextRunTime->format('Y-m-d H:i'));
        $schedule->setNextRuntime($nextRunTime);
        $schedule->setLastRuntime(new \DateTime($lastRunTime));
        $this->entityManager->persist($schedule);
        $this->logger->info('Last run time: '.$schedule->getLastRuntime()->format('Y-m-d H:i'));
    }

    private function generateNextRunTime( $currentRunTime, $frequency): \DateTime{
        $nextRunTime = new \DateTime($currentRunTime);
        switch (strtoupper($frequency)){
            case 'DAILY':
                $nextRunTime = $nextRunTime->add(new \DateInterval('P1D'));
                break;
            case 'MONTHLY':
                $nextRunTime = $nextRunTime->add(new \DateInterval('P1M'));
                break;
            case 'HOURLY':
                $nextRunTime = $nextRunTime->add(new \DateInterval('PT1H'));
                break;
            default:
                break;
        }
        return $nextRunTime;
    }

    private function createEmail($action): Email{
        $email = (new Email())
            ->from('tiko@tkbean.co.za')
            ->to('tbanyini@gmail.com')
            ->subject($action)
            ;

        switch ($action){
            case 'SEND.EMAIL.GREETING':
                $email->html("<p>This is a greeting message</p>");
                break;
            case 'SEND.EMAIL.GOODBYE':
                $email->html("<p>This is a goodbye message</p>");
                break;
            default:
                break;
        }
        return $email;
    }

    private function getActions($actionTime): array {
        return  $this->entityManager->getRepository(Schedule::class)->findBy(
            ['next_runtime' => new \DateTime($actionTime) ]
        );
    }

    private function getScheduleByCompanyAndScheduleCode($companyCode, $scheduleCode) : ?Schedule{
        return  $this->entityManager->getRepository(Schedule::class)->findOneBy(
            [
                'company_code' => $companyCode,
                'schedule_code' => $scheduleCode
                ]
        );
    }





}
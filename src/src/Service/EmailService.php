<?php


namespace App\Service;


use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailService
{

    private $mailer;
    public function __construct(MailerInterface  $mailer)
    {
        $this->mailer = $mailer;

    }

    public function sendTestEmail(){
        $email = (new Email())
            ->from('tiko@tkbean.co.za')
            ->to('tbanyini@gmail.com')
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')
            ->text('Sending emails is fun again!')
            ->html('<p>See Twig integration for better HTML integration!</p>');

        $this->mailer->send($email);
    }

    /**
     * @param Email $email
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function send(Email $email){
        $this->mailer->send($email);
    }

}
<?php
namespace App\Service;

use Psr\Log\LoggerInterface;

class WorkerService {

    private $logger;

    public function __construct(LoggerInterface $logger){
        $this->logger = $logger;
    }

    public function initWorkers(){
        $this->logger->info('WorkerService initWorkers()..');
    }
}
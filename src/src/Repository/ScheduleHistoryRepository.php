<?php

namespace App\Repository;

use App\Entity\ScheduleHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ScheduleHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScheduleHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScheduleHistory[]    findAll()
 * @method ScheduleHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScheduleHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScheduleHistory::class);
    }

    // /**
    //  * @return ScheduleHistory[] Returns an array of ScheduleHistory objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ScheduleHistory
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
